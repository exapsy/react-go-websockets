package main

import (
	"gitlab.com/exapsy/react-go-websockets/example1/internal/app"
)

func main() {
	app.Run()
}