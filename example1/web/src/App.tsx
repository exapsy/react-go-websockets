import './App.css'

import React from 'react'

interface IMessage {
  date: number
  message: string
}

// Convert UTC number to Date string
const convertToDate = (date: number): string => {
  return `${new Date(date).getHours()}:${new Date(date).getMinutes()}:${new Date(date).getSeconds().toString().padStart(2, '0')}`
}

// const App: React.FC = () => {
//   // Add event listener to websocket
//   useEffect(() => {
//     console.log('Running useEffect');
//     // Websocket message handler
//     const handler = (e: MessageEvent) => {
//       setMessages(messages.concat({ date: Date.now(), message: e.data }))
//     }
//     // Listening to messages
//     ws.addEventListener(
//       'message',
//       handler,
//     )
//     return () => ws.removeEventListener('message', handler)
//   }, [])
//   // State to store messages
//   const [messages, setMessages] = useState<IMessage[]>([])
//   // Styles
//   const styles = {
//     messageBox: {
//       background: '#212121',
//       height: '100%',
//     },
//     message: {
//       display: 'inline-block',
//       width: '100%',
//       color: 'white',
//       padding: '8px'
//     },
//   }
//   // Render
//   return (
//     <div className='App'>
//       <div style={styles.messageBox}>
//         {messages.map(
//           (message, index) => (
//             <div style={styles.message} key={index}>
//               {`${convertToDate(message.date)} : ${message.message}`}
//             </div>
//           )
//         )}
//       </div>
//     </div>
//   );
// }

class App2 extends React.Component<{}, { messages: IMessage[] }> {

  ws = new WebSocket('ws://localhost:8081/ws')

  constructor(props: any) {
    super(props)

    // Listening to messages
    const handler = (e: MessageEvent) => {
      this.setState({
        messages: (this.state.messages.concat({ date: Date.now(), message: e.data }))
      })
    }

    this.ws.addEventListener(
      'message',
      handler,
    )

    this.state = {
      messages: []
    }
  }

  render() {
    // Styles
    const styles: { [key: string]: React.CSSProperties } = {
      messageBox: {
        background: '#212121',
        height: '100%',
      },
      message: {
        display: 'inline-block',
        width: '100%',
        color: 'white',
        fontSize: '1.2em',
        padding: '8px',
      },
      messageDate: {
        color: '#bbb',
        fontSize: '0.8em',
        float: 'left',
      },
      borderBottom: {
        paddingBottom: '2px',
        borderBottom: '1px solid #666',
      },
    }
    // Render
    return (
      <div className='App'>
        <div style={styles.messageBox}>
          {this.state.messages.map(
            (message, index) => (
              <div style={styles.message} key={index}>
                <span style={styles.messageDate}>
                  {`${convertToDate(message.date)}`}
                </span>
                <span style={styles.borderBottom}>
                  {`${message.message}`}
                </span>
              </div>
            )
          )}
        </div>
      </div>
    );
  }
}

export default App2;
