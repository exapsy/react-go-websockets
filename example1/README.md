# Example 1
## Instructions
1. Run this on `./example1`
```
yarn start
```
2. Go to http://localhost:3000
3. Inspect at server messages that come via the websocket.