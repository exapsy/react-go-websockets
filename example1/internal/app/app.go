package app

import (
	"net/http"
	"log"
	"time"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize: 1024,
	WriteBufferSize: 1024,
}

func websocketHandler(w http.ResponseWriter, r *http.Request) {
	// All origins allowed
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	// Upgrade communication
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("upgrade:", err)
		return
	}
	// Server sending message
	go func(conn *websocket.Conn) {
		defer conn.Close()
		tick := time.Tick(time.Second * 5)
		for range tick {
			err = conn.WriteMessage(1, []byte("Message from server"))
			if err != nil {
				log.Println("write:", err)
				conn.Close()
				break
			}
		}
	}(conn)
}

// Run executes the server
func Run() {
	// Handler
	http.HandleFunc("/ws", websocketHandler)
	// Run server
	log.Println("Running server ...")
	log.Fatal(http.ListenAndServe("localhost:8081", nil))
}