- [ReactJS & Golang Websockets](#ReactJS--Golang-Websockets)
  - [Directory](#Directory)
  - [Examples Layout](#Examples-Layout)
    - [What you should look into](#What-you-should-look-into)
    - [Directories](#Directories)
    - [Files](#Files)
  - [How to run](#How-to-run)

# ReactJS & Golang Websockets
This project contains a bunch of examples that use the **websocket technology** for *bi-directional asychronous communication* between the Server and the Client.

## Directory
- **Example 1** - Server sending message
- **Example 2** - Client sending message

## Examples Layout

### What you should look into
- **./web/src/app.tsx** - React Frontend application
- **./internal/app/app.go** - Golang Backend application

### Directories
- **./internal/app** - Golang server
- **./web** - ReactJS client
- **./vendor** - Contains all Go modules

### Files
- **./main.go** - Golang server root file, run by `go run ./main.go`
- **./go.mod** - Dependencies and project name
- **./go.sum** - All dependencies and sub-depedencies

## How to run
1. Git clone 
```
git clone git@gitlab.com:exapsy/react-go-websockets.git
```

2. **C**hange **D**irectory into one of the examples 
```bash
cd ./example1
```
3. Run server and frontend by 
```
yarn start
```