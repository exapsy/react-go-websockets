# Example 2
## Instructions
1. Run this on `./example1`
```
yarn start
```
2. Go to http://localhost:3000
3. Write a message in the textbox and send it (*by pressing enter*)
4. Inspect the server console (***hint**: you should see your messages in the console*)