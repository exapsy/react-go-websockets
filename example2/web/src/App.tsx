import './App.css'

import React from 'react'

interface IMessage {
  date: number
  message: string
}

const ws = new WebSocket('ws://localhost:8081/ws')
class App2 extends React.Component<{}, { messages: IMessage[] }> {

  constructor(props: any) {
    super(props)

    this.state = {
      messages: []
    }
  }

  onInputKeyPress(e: React.KeyboardEvent<HTMLInputElement>) {

    const { value: msg } = e.currentTarget
    if (e.key === 'Enter' && msg !== '') {
      ws.send(msg)
      e.currentTarget.value = ''
    }
  }

  render() {
    // Styles
    const styles: { [key: string]: React.CSSProperties } = {
      Input: {
        width: '80%',
        padding: '8px',
      },
    }
    // Render
    return (
      <div className='App'>
        <input
          placeholder='Write a message and press Enter'
          onKeyPress={this.onInputKeyPress}
          style={styles.Input}
        />
      </div>
    );
  }
}

export default App2;
